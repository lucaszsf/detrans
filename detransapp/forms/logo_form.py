from django import forms
from detransapp.models.sistema import Sistema

class LogoForm(forms.ModelForm):
    class Meta:
        model = Sistema
        fields = ['sigla', 'nome_completo', 'logo',]