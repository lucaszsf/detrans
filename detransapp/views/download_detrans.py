# coding: utf-8
import os
import mimetypes
# Uplouad
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, render
from detransapp.models.config_sinc import ConfigSinc
from django.views.generic.base import View
from detransapp.forms.up_apk import UploadApkForm
from django.http import StreamingHttpResponse
from django.core.servers.basehttp import FileWrapper
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.conf import settings

'''class FileIterWrapper(object):
    def __init__(self, flo, chunk_size = 1024**2):
        self.flo = flo
        self.chunk_size = chunk_size

    def next(self):
        data = self.flo.read(self.chunk_size)
        if data:
            return data
        else:
            raise StopIteration

    def __iter__(self):
        return self'''


class DownloadDetransView(APIView):
    permission_classes = (IsAuthenticated, AllowAny)
    # @method_decorator(login_required)
    # @method_decorator(validar_imei())
    # @method_decorator(registro_log_sinc(0))

    
    def get(self, request):
        
        filename = 'detrans.sqlite.gz'
        db_path = "%s/%s" % (settings.MEDIA_ROOT, filename)

        response = StreamingHttpResponse(FileWrapper(open(db_path)), content_type=mimetypes.guess_type(db_path)[0])

        response['Content-Type'] = 'application/x-gzip'
        response['Content-Length'] = os.path.getsize(db_path)
        response['Content-Disposition'] = "attachment; filename=%s" % filename

        return response

        '''

        print 'dbfile.size : ',dbfile.size
        #return HttpResponse(FileIterWrapper(dbfile))
        response = HttpResponse(FileIterWrapper(dbfile))
        response['Content-Type'] = 'application/x-sqlite3'
        response['Content-Disposition'] = 'attachment; filename=%s' % "detrans.sqlite"
        #response['Content-Length'] = len(response.content)
        #response['Content-Length'] = os.path.getsize(db_path)'''

        return response


class UploadDetransApkView(APIView):
    permission_classes = (IsAuthenticated, AllowAny)
    template = 'upload/apk.html'
    
    def get(self, request):
        cf = ConfigSinc.objects.filter()
        cf = cf[len(cf)-1]

        down = '0'
        form = UploadApkForm()
        if cf.caminho_apk != '':
            down = '1'
        return render(request, self.template, {'form': form, 'down': down},)

    def post(self, request):
        try:
            sistema = ConfigSinc.objects.filter()[0]
        except:
            return HttpResponseRedirect('/download-apk/')
            
        form = UploadApkForm(request.POST or None, request.FILES or None, instance=sistema)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect('/')
        else:
            
            form = UploadApkForm()
            return render(request, self.template, {'form': form})

def handle_uploaded_file(f):

    name = 'media/' + str(f.name)
    destination = open(name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()

class DownloadDetransApkView(View):
    permission_classes = (IsAuthenticated, AllowAny)
      
    #Download    
    def get(self, request, down=None):
        print "chegou post"
        cf = ConfigSinc.objects.filter()
        cf = cf[len(cf)-1]
        if down == '1':
            filename = str(cf.caminho_apk)

            if filename == 'None':
                
                return HttpResponseRedirect('/download-apk/')


            db_path = "%s/%s" % (settings.MEDIA_ROOT, filename)

            response = StreamingHttpResponse(FileWrapper(open(db_path)), content_type=mimetypes.guess_type(db_path)[0])

            response['Content-Type'] = 'application/vnd.android.package-archive'
            response['Content-Length'] = os.path.getsize(db_path)
            response['Content-Disposition'] = "attachment; filename=%s" % filename

            return response
        else:
            pass 
