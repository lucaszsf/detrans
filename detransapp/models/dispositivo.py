from django.db import models
from detransapp.manager import DispositivoManager


class Dispositivo(models.Model):
    
    
    imei = models.BigIntegerField(unique=True)

    ativo = models.BooleanField(default=True)

    objects = DispositivoManager()

	
    class Meta:
    	app_label = "detransapp"

	def __unicode__(self):
		return self.imei
