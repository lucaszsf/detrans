from django import forms
from detransapp.models.config_sinc import ConfigSinc

class UploadApkForm(forms.ModelForm):
    class Meta:
    	model = ConfigSinc
    	fields = ['caminho_apk',]