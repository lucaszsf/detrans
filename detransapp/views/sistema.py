# -*- coding:utf-8 -*-
from django.views.generic.base import View
from rest_framework.permissions import IsAuthenticated, AllowAny
from detransapp.forms.logo_form import LogoForm
from django.shortcuts import render, redirect
from detransapp.models.sistema import Sistema
from datetime import datetime

class UploadDetransLogoView(View):

	template = 'upload/logo.html'

	def get(self, request, sistema_id=None):

		sistema = Sistema.objects.filter()
		if len(sistema) > 0:
			sistema_id = sistema[0].id

		if sistema_id:
			sistema = Sistema.objects.get(pk=sistema_id)
			form = LogoForm(instance=sistema)

		else:
			form = LogoForm()

		return render(request, self.template, {'form': form})

	def post(self, request, sistema_id=None):

		sistema = Sistema.objects.filter()
		
		if len(sistema) > 0:
			sistema_id = sistema[0].id       
			

		# request.POST['logo'] = 'images/' + str(request.FILES['logo'].name)
		if sistema_id:
			sistema = Sistema.objects.get(pk=sistema_id) 
			form = LogoForm(request.POST or None, request.FILES or None, instance=sistema)
			
		else:
			form = LogoForm(request.POST or None, request.FILES or None)
		

		print form.errors

		if form.is_valid():

			form.save(request)
		
			return redirect('/')

		else:
			
			return redirect('/sistema/')



	
		
		